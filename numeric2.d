/*
     Compute heights in centimeters for a range of heights
       expressed in feet and inches
       */
import std.stdio;

void main() {
    // values unlikely to change soon
    immutable int inchPerFoot = 12;
    immutable double cmPerInch = 2.54;

    // loop'n write
    foreach (feet; 5 .. 7) {
        foreach (inches; 0 .. inchPerFoot) {
            writefln("%s'%s''\t%s", feet, inches, (feet * inchPerFoot + inches) * cmPerInch);
        }
    }
}
