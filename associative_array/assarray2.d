import std.stdio, std.string,std.array,std.algorithm;

void main() {
    // compute counts
    uint[string] freqs;
    foreach(line;stdin.byLine){
        string[] words = cast(string[]) split(strip(line));
        foreach(word; words){
            writeln(word);
        }
        foreach (word;words){ 
            ++freqs[word];
        }
    }
    string[] words = array(freqs.keys);
    //sort!((a,b){return freqs[a]>freqs[b];})(words);
    words.sort();
    // print counts
    foreach (key, value; freqs) {
        writefln("%6u\t%s", value, key);
    }
    writeln("\n");
    foreach(word;words){
        writefln("%6u\t%s",freqs[word],word);
    }
    writeln("\n");
    sort!((a,b){return freqs[a]>freqs[b];})(words);
    foreach(word;words){
        writefln("%6u\t%s",freqs[word],word);
    }
    writeln("\n");
}
