import std.stdio, std.string;

void main() {
    uint[string] dic;
    foreach (line; stdin.byLine) {
        // Break sentence into words
        string[] words = cast(string[])split(strip(line));
        // Add each word in the sentence to the vocabulary
        foreach (word; words) {
            if (word in dic) continue; // nothing to do
            uint newID = cast(int) dic.length;
            dic[word] = newID;
            writeln(newID, '\t', word);
        }
    }
}
